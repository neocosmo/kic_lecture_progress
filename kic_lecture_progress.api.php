<?php

/**
 * @file
 * Hook definitions of kic_lecture_progress.
 */

/**
 * Define the progress types available.
 *
 * @return array
 *   An array of progress type names.
 */
function hook_kic_lecture_progress_types() {
  return ['read-progress', 'exercise-progress'];
}
