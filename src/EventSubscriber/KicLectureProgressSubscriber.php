<?php

namespace Drupal\kic_lecture_progress\EventSubscriber;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\kic_lecture_progress\KicLectureProgressFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;

/**
 * KI-Campus Lecture Progress Event Subscriber.
 */
class KicLectureProgressSubscriber implements EventSubscriberInterface {

  /**
   * A progress factory.
   *
   * @var \Drupal\kic_lecture_progress\KicLectureProgressFactory
   */
  protected $progressFactory;

  /**
   * A user proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::FINISH_REQUEST => 'kernelFinishRequest',
    ];
  }

  /**
   * Constructor.
   *
   * @param \Drupal\kic_lecture_progress\KicLectureProgressFactory $progress_factory
   *   A progress factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   A user proxy.
   */
  public function __construct(
    KicLectureProgressFactory $progress_factory,
    AccountProxyInterface $user) {

    $this->progressFactory = $progress_factory;
    $this->user = $user;
  }

  /**
   * Callback for kernel finish request events.
   */
  public function kernelFinishRequest() {
    if ($this->user->isAnonymous()) {
      return;
    }
    $progress = $this->progressFactory->get($this->user->getAccount());
    $progress->load();
    $types = kic_lecture_progress_types();
    /*
     * Store the current lecture progress in a cookie. We do this on every page
     * load, because we cannot be sure, that the correct cookie data is still
     * there. It could be, that the user closed the browser (the cookie lasts
     * only for a session), opens it again and still is logged in. Or the user
     * initially did not accept the correct cookies, but later changed his
     * settings.
     * Rather than reacting to all those special events, we set the cookie at
     * every page load.
     */
    foreach ($types as $type) {
      $progress->storeInCookie($type);
    }
  }

}
