<?php

namespace Drupal\kic_lecture_progress;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * KI-Campus Lecture Progress Factory.
 */
class KicLectureProgressFactory implements ContainerInjectionInterface {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbconn;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('database'),
      $container->get('logger.factory')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $dbconn
   *   A database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory.
   */
  public function __construct(
    Connection $dbconn,
    LoggerChannelFactoryInterface $logger_factory) {

    $this->dbconn = $dbconn;
    $this->logger = $logger_factory->get('kic_lecture_progress');
  }

  /**
   * Get a lecture progress object for a specifiy user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user for whom the get the progress.
   *
   * @return \Drupal\kic_lecture_progress\KicLectureProgress
   *   The user's lecture progress.
   */
  public function get(AccountInterface $user) {
    return new KicLectureProgress($this->dbconn, $user, $this->logger);
  }

}
