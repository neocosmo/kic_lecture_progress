<?php

namespace Drupal\kic_lecture_progress;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * KI-Campus Lecture Progress of a single user.
 */
class KicLectureProgress {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbconn;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The lecture progress, keyed by type.
   *
   * @var array
   */
  protected $progress;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $dbconn
   *   A database connection.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The currently logged in user.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   A logger factory.
   */
  public function __construct(
    Connection $dbconn,
    AccountInterface $user,
    LoggerChannelInterface $logger) {

    $this->dbconn = $dbconn;
    $this->user = $user;
    $this->logger = $logger;
    $this->progress = [];
  }

  /**
   * Load the progress from the database.
   */
  public function load() {
    $uid = $this->user->id();
    $data = $this->dbconn
      ->select('kic_lecture_progress')
      ->fields('kic_lecture_progress', ['type', 'nid'])
      ->condition('uid', $uid, '=')
      ->execute()
      ->fetchAll();
    foreach ($data as $p) {
      $this->progress[$p->type][] = $p->nid;
    }
  }

  /**
   * Get the progress for a specifiy type.
   *
   * If the progress is not already loaded, it is retrieved from the database.
   *
   * @param string $type
   *   The progress type.
   *
   * @return array
   *   An array of node IDs indicating the progress.
   */
  public function get(string $type) {
    if (!isset($this->progress[$type]) && !empty($type)) {
      $this->load();
    }
    return $this->progress[$type];
  }

  /**
   * Merge the current progress with new progress.
   *
   * This does not automtically load the current progress from the database.
   * Neither does it store the merged progress to the database.
   *
   * @param string $type
   *   The progress' type.
   * @param array $progress
   *   An array of node IDs indicating the progress.
   */
  public function merge(string $type, array $progress) {
    if (!isset($this->progress[$type])) {
      $this->progress[$type] = $progress;
    }
    else {
      $this->progress[$type] = array_merge($this->progress[$type], $progress);
      $this->progress[$type]
        = array_unique($this->progress[$type], SORT_NUMERIC);
    }
  }

  /**
   * Save a user's lecture progress into the database.
   */
  public function save() {
    $uid = $this->user->id();
    $transaction = $this->dbconn->startTransaction();
    try {
      foreach ($this->progress as $type => $progress) {
        // Delete all values currently present.
        $this->dbconn->delete('kic_lecture_progress')
          ->condition('uid', $uid, '=')
          ->condition('type', $type, '=')
          ->execute();
        // Insert all values.
        $insert_query = $this->dbconn->insert('kic_lecture_progress')
          ->fields(['uid', 'nid', 'type']);
        foreach ($progress as $nid) {
          $insert_query->values([
            'uid' => $uid,
            'nid' => $nid,
            'type' => $type,
          ]);
        }
        $insert_query->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollBack();
      $this->logger->error($e->getMessage());
    }
    // The transaction is automatically committed, when the $transaction
    // variable goes out of scope.
  }

  /**
   * Store the current progress in cookies.
   *
   * @param string $type
   *   Optional. If specified, only the progress of that type is stored in a
   *   cookie.
   */
  public function storeInCookie(string $type = '') {
    if (empty($type)) {
      foreach ($this->progress as $type => $progress) {
        setcookie($type, json_encode($progress), 0, '/');
      }
    }
    elseif (isset($this->progress[$type])) {
      setcookie($type, json_encode($this->progress[$type]), 0, '/');
    }
  }

}
