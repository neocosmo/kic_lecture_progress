<?php

namespace Drupal\kic_lecture_progress\Controller;

use Drupal\kic_lecture_progress\KicLectureProgressFactory;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for KIC-API requests.
 */
class KicLectureProgressController extends ControllerBase {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbconn;

  /**
   * Lecture progress factory.
   *
   * @var \Drupal\kic_lecture_progress\KicLectureProgressFactory
   */
  protected $progressFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('kic_lecture_progress.factory')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $dbconn
   *   A database connection.
   * @param \Drupal\kic_lecture_progress\KicLectureProgressFactory $progress_factory
   *   A lecture progress factory.
   */
  public function __construct(
    Connection $dbconn,
    KicLectureProgressFactory $progress_factory) {

    $this->dbconn = $dbconn;
    $this->progressFactory = $progress_factory;
  }

  /**
   * Route callback for saving a user's progress.
   */
  public function save(AccountInterface $user, Request $request) {
    if (!$this->currentUser()->hasPermission('post any lecture progress')
      && $this->currentUser->id() !== $user->id()) {

      return JsonResponse::create(NULL, 403);
    }
    $type = $request->get('type');
    $progress = json_decode($request->get('progress', '[]'));

    $progress_handler = $this->progressFactory->get($user);
    $progress_handler->get($type);
    $progress_handler->merge($type, $progress);
    $progress_handler->save();

    return JsonResponse::create(['title' => 'Success!']);
  }

}
