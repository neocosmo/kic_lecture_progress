<?php

/**
 * @file
 * KI-Campus Lecture progress module.
 */

use Drupal\user\UserInterface;

/**
 * Implements hook_user_login().
 */
function kic_lecture_progress_user_login(UserInterface $account) {
  $progress = \Drupal::service('kic_lecture_progress.factory')->get($account);
  $progress->load();
  $types = kic_lecture_progress_types();
  $request = \Drupal::requestStack()->getCurrentRequest();
  foreach ($types as $type) {
    $cookie_data = $request->cookies->get($type);
    if (empty($cookie_data)) {
      continue;
    }
    $cookie_progress = json_decode($cookie_data);
    if (!is_array($cookie_progress)) {
      continue;
    }
    $progress->merge($type, $cookie_progress);
    $progress->storeInCookie($type);
  }
  $progress->save();
}

/**
 * Implements hook_user_logout().
 */
function kic_lecture_progress_user_logout() {
  $types = kic_lecture_progress_types();
  foreach ($types as $type) {
    setcookie($type, '', 1, '/');
  }
}

/**
 * Get the lecture progress types.
 *
 * @return array
 *   An array of progres types.
 */
function kic_lecture_progress_types() {
  $types = &drupal_static(__FUNCTION__, []);
  $theme = \Drupal::service('theme.manager')->getActiveTheme();
  $theme_name = $theme->getName();

  if (isset($types[$theme_name])) {
    return $types[$theme_name];
  }

  $cache = \Drupal::cache()->get(__FUNCTION__);
  if ($cache !== FALSE) {
    $types = $cache->data;
    if (isset($types[$theme_name])) {
      return $types[$theme_name];
    }
  }

  $types[$theme_name] = \Drupal::moduleHandler()->invokeAll('kic_lecture_progress_types');
  // Allow themes to define lecture progress types.
  $themes = [$theme];
  $themes = array_merge($themes, $themes[0]->getBaseThemeExtensions());
  foreach ($themes as $theme) {
    $name = $theme->getName();
    if (function_exists($name . '_kic_lecture_progress_types')) {
      $new_types = call_user_func($name . '_kic_lecture_progress_types');
      if (is_array($new_types)) {
        $types[$theme_name] = array_merge($types[$theme_name], $new_types);
      }
      else {
        $types[$theme_name][] = $new_types;
      }
    }
  }
  $types[$theme_name] = array_unique($types[$theme_name]);
  \Drupal::cache()->set(__FUNCTION__, $types);
  return $types[$theme_name];
}
